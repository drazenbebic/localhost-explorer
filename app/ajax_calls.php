<?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if ((isset($_POST["cms"])) && (isset($_POST["target"]))) {
            extractCMS($_POST["target"], $_POST["cms"]);
            exit();
        }
        if (isset($_POST["loadStructure"])) {
            loadStructure();
            include_once("app/ajax/load_structure.php");
            exit();
        }
        if (isset($_POST["folderName"])) {
            createFolder($_POST["folderName"]);
            exit();
        }
        if (isset($_POST["deleteFolder"])) {
            deleteFolder($_POST["deleteFolder"]);
            exit("success");
        }
        if (isset($_POST["oldName"]) && isset($_POST["newName"])) {
            renameFolder($_POST["oldName"], $_POST["newName"]);
            exit("success");
        }
        if (isset($_POST["char"]) && isset($_POST["folders"])) {
            loadStructure();
            $folders = searchFolders($_POST["char"], $_POST["folders"]);
            include_once("app/ajax/load_folders.php");
            exit();
        }
    }
?>