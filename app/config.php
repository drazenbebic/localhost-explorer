<?php

    /* global file/folder structure variables */
    $folders        = array();
    $files          = array();
    $dir_structure  = array();
    $file_structure = array();

    /* basic directories */
    define("APP_ROOT", "app");
    define("DIR_ROOT", getcwd());

    /* CMS remote URL's */
    define("WP_URL", "https://wordpress.org/latest.zip");
    define("JOOMLA_URL", "https://downloads.joomla.org/cms/joomla3/3-6-5/joomla_3-6-5-stable-full_package-zip?format=zip");
    define("DRUPAL_URL", "https://ftp.drupal.org/files/projects/drupal-8.2.7.zip");
    define("TYPO3_URL", "https://downloads.sourceforge.net/project/typo3/TYPO3%20Source%20and%20Dummy/TYPO3%207.6.18/typo3_src-7.6.18.zip?r=&ts=1493667880&use_mirror=freefr");

?>