<?php

    /**
     * [extracts the latest cms installation into the target directory]
     * @param string $dir
     * @param string $cms
     */
    function extractCMS($dir, $cms) {
        /*
        $zip = new ZipArchive();
        switch ($cms) {
            case 'WordPress':
                $zip_remote = WP_URL;
                $zip_local  = APP_ROOT . "/cms/wordpress-latest.zip";
                break;
            case 'Joomla!':
                $zip_remote = JOOMLA_URL;
                $zip_local  = APP_ROOT . "/cms/joomla-latest.zip";
                break;
            case 'Drupal':
                $zip_remote = DRUPAL_URL;
                $zip_local  = APP_ROOT . "/cms/drupal-latest.zip";
                break;
            case 'TYPO3':
                $zip_remote = TYPO3_URL;
                $zip_local  = APP_ROOT . "/cms/typo3-latest.zip";
                break;
        }

        copy($zip_remote, $zip_local);
        if ($zip->open($zip_local)) {
            $zip->extractTo($dir);
            $zip->close();
        }

        $structure = array_diff(scandir($dir), array(".", ".."));
        if (count($structure) == 1) {
            if (is_dir(DIR_ROOT . "/" . $dir . "/" . $structure[2])) {
                recursiveCopy($dir . "/" . $structure[2], $dir);
                recursiveDelete($dir . "/" . $structure[2], $dir);
            }
        }
        */
        sleep(2);
    }

    /* copies folder and all of its contents from $src to $dst */
    function recursiveCopy($src, $dst) {
        $dir = opendir($src);
        while (false !== ($entry = readdir($dir))) {
            if (($entry != ".") && ($entry != "..")) {
                if (is_dir($src . "/" . $entry)) {
                    recursiveCopy($src . "/" . $entry, $dst . "/" . $entry);
                } else {
                    if (!file_exists($dst . "/")) {
                        mkdir($dst . "/", 0755, true);
                    }
                    copy($src . "/" . $entry, $dst . "/" . $entry);
                }
            }
        }
        closedir($dir);
    }

    /* deletes all content inside $dst (all files and folders!) */
    function recursiveDelete($dst) {
        $dir = opendir($dst);
        while (false !== ($entry = readdir($dir))) {
            if (($entry != ".") && ($entry != "..")) {
                if (is_dir($dst . "/" . $entry)) {
                    recursiveDelete($dst . "/" . $entry);
                } else {
                    unlink($dst . "/" . $entry);
                }
            }
        }
        closedir($dir);
        rmdir($dst);
    }

    /* loads the file structure for the main view */
    function loadStructure() {
        $folders     = array();
        $files       = array();
        $helper_dir  = array();
        $helper_file = array();

        $dir = new DirectoryIterator(DIR_ROOT);

        foreach ($dir as $item) {
            if (!$dir->isDot() && strval($item) != ".git" && strval($item) != APP_ROOT) {
                if ($dir->isDir()) {
                    $folders[] = strval($item);
                    $subdir = new DirectoryIterator(DIR_ROOT . "/" . $item);
                    foreach ($subdir as $subitem) {
                        if (!$subdir->isDot()) {
                            if ($subdir->isDir()) {
                                array_push($helper_dir, "<i class='fa fa-folder' aria-hidden='true'></i>&nbsp;&nbsp;" . strval($subitem));
                            } elseif ($subdir->isFile()) {
                                array_push($helper_file, "<i class='fa fa-file' aria-hidden='true'></i>&nbsp;&nbsp;" . strval($subitem));
                            }
                        }
                    }
                    $dir_structure[strval($item)]  = $helper_dir;
                    $file_structure[strval($item)] = $helper_file;
                    $helper_dir                    = array();
                    $helper_file                   = array();
                } elseif ($dir->isFile())
                    if (strval($item) != "index.php" && strval($item) != "README.md") {
                        $helper_file         = stat($item);
                        $helper_file["name"] = strval($item);
                        $helper_file         = array_slice($helper_file, 13);
                        array_push($files, $helper_file);
                        $helper_file = array();
                    }
            }
        }

        $GLOBALS["folders"]        = $folders;
        $GLOBALS["files"]          = $files;
        $GLOBALS["dir_structure"]  = $dir_structure;
        $GLOBALS["file_structure"] = $file_structure;
    }

    /* creates a new folder */
    function createFolder($name) {
        loadStructure();
        if (in_array($name, $GLOBALS["folders"])) {
            exit("folder-exists");
        } else {
            mkdir(DIR_ROOT . "/" . $name);
            chmod(DIR_ROOT . "/" . $name, 0777);
            exit("success");
        }
    }

    /* deletes a folder */
    function deleteFolder($name) {
        if (file_exists(DIR_ROOT . "/" . $name)) {
            recursiveDelete($name);
            loadStructure();
            exit("success");
        } else {
            exit("doesnt-exist");
        }
    }

    /* renames a folder */
    function renameFolder($old_name, $new_name) {
        if (file_exists(DIR_ROOT . "/" . $old_name)) {
            rename(DIR_ROOT . "/" . $old_name, DIR_ROOT . "/" . $new_name);
        }
    }

    /* format file size units */
    function formatSizeUnits($bytes) {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' kB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }
        return $bytes;
    }

    /* convert date from UNIX Timestamp to desired format */
    function convertDate($date, $format = "Y/m/d H:i:s") {
        return date($format, $date);
    }

    /* var_dump with preformatted text */
    function var_dump_pre($mixed = null) {
        echo '<pre>';
        var_dump($mixed);
        echo '</pre>';
        return null;
    }

    function searchFolders($char, $folders) {
        $folders = $_POST["folders"];
        $char    = $_POST["char"];
        $result  = array();

        if ($char == "") {
            $result = $folders;
        } else {
            foreach ($folders as $folder) {
                if(stristr($folder, $char)) {
                    array_push($result, $folder);
                }
            }
        }

        return $result;
    }

?>