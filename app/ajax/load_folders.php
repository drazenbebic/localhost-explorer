<?php if(!empty($folders)): ?>
<ul class="ul-folders">
	<?php foreach($folders as $folder): ?>
		<li data-name="<?=$folder;?>">
			<i class="fa fa-folder" aria-hidden="true"></i>
			<span><?=$folder;?></span>
		</li>
	<?php endforeach; ?>
</ul>
<?php else: ?>
<div class="div-folders-empty">
	<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
	<p>No such folder exists.</p>
</div>
<?php endif; ?>