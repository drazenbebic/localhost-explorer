<div class="div-folders">
	<div class="div-body-title">Folders</div>
	<?php foreach($folders as $folder): ?>
		<div class="div-folder-wrapper" data-name="<?=$folder?>">
			<?php if(empty($dir_structure[$folder]) && empty($file_structure[$folder])): ?>
				<div class="div-folder-empty">
					<div class="div-folder-empty-icon"><i class="fa fa-code" aria-hidden="true"></i></div>
				</div>
			<?php else: ?>
				<div class="div-folder-nonempty">
					<ul class="ul-directory">
						<?php foreach($dir_structure[$folder] as $item): ?>
							<li><?=$item?></li>
						<?php endforeach; ?>
						<?php foreach($file_structure[$folder] as $item): ?>
							<li><?=$item?></li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>
			<div class="div-folder-title" title="<?=$folder?>">
				<span><?=$folder?></span>
				<div><input type="text" name="folder-title"></div>
			</div>
			<div class="div-folder-actions">
				<button class="btn-ripple btn-ripple-icor" data-action="rename"><i class="fa fa-font" aria-hidden="true"></i>Rename</button>
				<button class="btn-ripple btn-ripple-icor" data-action="delete"><i class="fa fa-times" aria-hidden="true"></i>Delete</button>
			</div>
		</div>
	<?php endforeach; ?>
</div>

<div class="div-files">
	<div class="div-body-title">Files</div>
	<div class="div-files-table">
		<div class="div-files-header">
			<div class="div-files-cell">Name</div>
			<div class="div-files-cell">Size</div>
			<div class="div-files-cell">Modified</div>
		</div>
		<?php foreach($files as $file): ?>
			<div class="div-files-row">
				<div class="div-files-cell"><?=$file["name"];?></div>
				<div class="div-files-cell"><?=formatSizeUnits($file["size"]);?></div>
				<div class="div-files-cell"><?=convertDate($file["ctime"]);?></div>
			</div>
		<?php endforeach; ?>
	</div>
</div>