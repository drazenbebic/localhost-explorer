/* global variables */
var folders = new Array();

/* global functions */
function loadStructure() {
	$.ajax({
		type: "POST",
		dataType: "text",
		data: {
			loadStructure: "1",
		},
		cache: false,
		success: function(data) {
			/* inserts the response data into the target div */
			$(".div-structure").html(data);

			/* folder action buttons */
			$(".div-folder-actions").children().click(function() {
				$(".div-folder-wrapper").removeAttr("data-selected");
				$(this).parents(".div-folder-wrapper").attr("data-selected", "true");

				var action = $(this).attr("data-action");
				if (action == "delete") {
					$(".div-fs-overlay[data-name='delete-folder']").fadeIn("400");
				} else if (action == "rename") {
					var titleParent = $(this).parents(".div-folder-wrapper").children(".div-folder-title");
					var oldName     = titleParent.children("span").html();
					titleParent.children("span").hide();
					titleParent.children("div").show()
					titleParent.children("div").children("input").val(oldName).select();
				}
			});

			/* rename folder input field */
			$(".div-folder-title div input").keydown(function(e) {
				if (e.keyCode == 13) {
					var oldName = $(this).parents(".div-folder-wrapper").children(".div-folder-title").children("span").html()
					if (oldName == $(this).val()) {
						$(this).parent().hide();
						$(this).parent().siblings("span").show();
					} else {
						renameFolder(oldName, $(this).val());
					}
				} if (e.keyCode == 27) {
					$(this).parent().hide();
					$(this).parent().siblings("span").show();
				}
			});

			/* fill global variable with folder names */
			folders = new Array();
			$(".div-folder-title").each(function(i, el) {
				folders.push($(this).children("span").html());
			});

		},
		error: function(data) {
			alert("ERROR!" + data);
		},
		complete: function() {
		}
	});
}

function deleteFolder(name) {
	$.ajax({
		type: "POST",
		data: {
			deleteFolder: name,
		},
		cache: false,
		success: function(data) {
			loadStructure();
			if (data == "doesnt-exist") {
				alert("The folder you selected doesn't exist\nAll folders have been reinitialized.");
			}
		},
		error: function(data) {
			console.log("error: " + data);
		},
		complete: function(data) {
		}
	});
}

function createFolder(name) {
	$.ajax({
		type: "POST",
		data: {
			folderName: name,
		},
		cache: false,
		success: function(data) {
			loadStructure();
			if (data == "folder-exists") {
				alert("Folder already exists");
			}
		},
		error: function(data) {
			console.log("error: " + data);
		},
		complete: function(data) {
			$(".load-new-folder").hide();
		}
	});
}

function renameFolder (oldName, newName) {
	$.ajax({
		type: "POST",
		data: {
			oldName: oldName,
			newName: newName,
		},
		cache: false,
		success: function(data) {
			console.log(data);
			loadStructure();
		},
		error: function(data) {
			console.log("error: " + data);
		},
		complete: function(data) {
		}
	});
}

function installCMS(cms, target) {
	$.ajax({
		type: "POST",
		data: {
			target: target,
			cms: cms,
		},
		cache: false,
		success: function(data) {
			console.log(data);
			loadStructure();
			$(".btn-standard[data-action='setup'] span").html(cms);
			$(".div-setup-step1").hide();
			$(".div-setup-step2").fadeIn("400");
		},
		error: function(data) {
			console.log("error: " + data);
		},
		complete: function(data) {
			$(".load-cms-install").hide();
		}
	});
}

function loadFolders(char, folders) {
	$.ajax({
		type: "POST",
		data: {
			char: char,
			folders: folders
		},
		cache: false,
		success: function(data) {
			$(".div-folders-dst").html(data);
			$(".ul-folders li").click(function() {
				var target = $(this).attr("data-name");
				$(".AJAXhelper").attr("data-target", target);
				$(".ul-folders li i").removeClass().addClass("fa fa-folder");
				$(this).children("i").removeClass().addClass("fa fa-check");
			});
		},
		error: function(data) {
			console.log("error: " + data);
		},
		complete: function(data) {
		}
	});
}

$(document).mouseup(function(e) {
	/* closes context menu if clicked outside of it */
	var container = $(".div-context-menu");
	if (!container.is(e.target) && container.has(e.target).length === 0) {
		container.hide();
	}
});

$(document).ready(function(){

	/* load initial folders on element load */
	$(".div-structure").on("initialLoad", function() {
		loadStructure();
	});
	$(".div-structure").trigger("initialLoad");

	/* open cms installation popup */
	$(".ul-cms-list li").click(function() {
		var cms = $(this).attr("data-cms");
		loadFolders("", folders);
		$(".AJAXhelper").attr("data-cms", cms);
		$(".div-setup-step1 > p > span").html(cms);
		$(".div-setup-step2").hide();
		$(".div-setup-step1").show();
		$(".div-fs-overlay[data-name='cms-install']").fadeIn("400");
		$(".ipt-dark[name='search-folders']").val("").focus();
	});

	/* menu actions */
	$(".ul-actions-list li").click(function() {
		var action = $(this).attr("data-action");
		if (action == "refresh") {
			loadStructure();
		} else if (action == "cfolder") {
			$(".div-fs-overlay[data-name='new-folder']").fadeIn("400");
			$(".ipt-standard[name='new-folder']").val("").focus();
		}
	});

	/* close overlay */
	$("[data-action='close']").click(function() {
		$(".div-folder-wrapper").removeAttr("data-selected");
		$(".div-fs-overlay").fadeOut("400");
	});

	/* install cms into target directory */
	$("[data-action='confirm']").click(function() {
		var cms    = $(".AJAXhelper").attr("data-cms");
		var target = $(".AJAXhelper").attr("data-target");

		if (!target) {
			alert("Please choose a folder.");
		} else {
			$(".load-cms-install").show();
			installCMS(cms, target);
		}
	});

	/* side navigation open/close */
	$(".div-menu-toggle").click(function() {
		if ($(".menu").css("width") == "50px") {
			$(".menu").css("width", "250px");
			$(".menu").css("padding", "50px 15px 0px 15px");
			$(".body").css("margin-left", "250px");
			$(".ul-tools-list > li, .ul-actions-list > li, .ul-cms-list > li").css("border-radius", "5px");
			$(".menu > .div-title-icon").hide();
			$(".menu > .div-title-text").fadeIn("400");
			$(this).children(".div-menu-icon").html("<i class='fa fa-times' aria-hidden='true'></i>");
		} else {
			$(".menu").css("width", "50px");
			$(".menu").css("padding", "50px 0px 0px 0px");
			$(".body").css("margin-left", "50px");
			$(".ul-tools-list > li, .ul-actions-list > li, .ul-cms-list > li").css("border-radius", "0px");
			$(".menu > .div-title-text").hide();
			$(".menu > .div-title-icon").fadeIn("400");
			$(this).children(".div-menu-icon").html("<i class='fa fa-bars' aria-hidden='true'></i>");
		}
	});

	/* opens the setup process in a new tab */
	$("[data-action='setup']").click(function() {
		var target = $(".AJAXhelper").attr("data-target");
		window.open(target, "_blank");
		$(".div-fs-overlay[data-name='cms-install']").hide();
	});

	/* create folder */
	$("[data-action='new-folder']").click(function() {
		var folderName = $(".ipt-standard[name='new-folder']").val();
		if (!folderName) {
			alert("Please enter the folder name.");
		} else {
			$(".load-new-folder").show();
			createFolder(folderName);
		}
	});
	$(".ipt-standard[name='new-folder']").keypress(function(e) {
		var folderName = $(".ipt-standard[name='new-folder']").val();
		if (e.keyCode == 13) {
			if (!folderName) {
				alert("Please enter the folder name.");
			} else {
				$(".load-new-folder").show();
				createFolder(folderName);
			}
		}
	});

	/* delete folder */
	$("[data-action='delete-folder']").click(function() {
		var name = $(".div-folder-wrapper[data-selected='true']").attr("data-name");
		deleteFolder(name);
		$(".div-fs-overlay[data-name='delete-folder']").hide();
	});

	/* open links on click */
	$("[data-link]").click(function() {
		var url = $(this).attr("data-href");
		window.open(url, '_blank');
	});

	/* create ripple effect on button click */
	$("[data-ripple]").click(function(e) {

		var $self = $(this);
		if ($self.is(".btn-disabled")) {
			return;
		}

		if ($self.closest("data-ripple")) {
			e.stopPropagation();
		}

		var initPos = $self.css("position"),
			offs = $self.offset();
			x = e.pageX - offs.left,
			y = e.pageY - offs.top,
			dia = Math.min(this.offsetHeight, this.offsetWidth, 100),
			$ripple = $("<div/>", {class:"ripple", appendTo:$self});

		if (!initPos || initPos == "static") {
			$self.css({position:"relative"});
		}

		$("<div/>", {
			class : "ripple-wave",
			css : {
				background: $self.data("ripple"),
				width: dia,
				height: dia,
				left: x - (dia/2),
				top: y - (dia/2),
			},
			appendTo: $ripple,
			one: {
				animationed: function() {
					$ripple.remove();
				}
			}
		});
	});

	/* search folders as you type */
	$(".ipt-dark[name='search-folders']").keyup(function(e) {
		var char = $(this).val();
		loadFolders(char, folders);
	});


});