# localhost Explorer
Visualizing the contents of localhost, with extra features.

Installation:
Put these files into your localhost root directory, after that go to http://localhost/

Features:
 - Custom file and directory listing.
 - Create and delete directories.
 - Refresh the directory.
 - Automated CMS installation (Drupal, Joomla!, WordPress, TYPO3).

Suggestions? Write in the comments!